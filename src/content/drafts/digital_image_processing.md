+++
title = "Digital Image Processing"
date = 2023-08-08
[taxonomies]
tags = ["computer science", "image processing", "algorithms"]
+++

## What is an image
image in essance is just a function. 
f(x,y) => intencity or gray level at each x and y coordinates

## What is digital image processing `DIP`
`DIP` is the process of manipulating the image or extracting attributes from it 

it could be categorized to 
    - Low level: where both inputs and outputs are images
        - noise reduction
        - color correction
        - image sharpening...
    - Mid level: where inputs are images and outputs are image attributes
        - segmentation of images 
        - object classification
    - High level: where inputs are images and outputs are higher level semantics description based on the understanding of the image
        - this falls more in the category of computer vision more than `DIP`


## The Origins of `DIP`
- The earliest applications go back to the early 1900s were the newspaper industry was trying to send images accross the atlantic ocean
- since computers only understands data 0s and 1s. they needed to a way to turn images to 0s and 1s `mid level processing`

STOPED at 7:20 https://youtu.be/rSGMXktIsYI

