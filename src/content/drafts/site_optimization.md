+++
title = "Site Optimization"
date = 2019-02-02
[taxonomies]
tags = ["SSG", "tailwind"]
+++

- Woff2 > woff > ttf

## Use webfont generator
download font from google since its already optimized
https://www.fontsquirrel.com/tools/webfont-generator
check ttf woff and woff2
vertical metrics: use no-adjustments
fix missing glyphs: uncheck all
use no subsetting
and download the font

