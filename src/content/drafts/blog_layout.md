+++
title = "Layout design"
date = 2023-08-08
[taxonomies]
tags = ["design", "ui", "ux"]
+++

## Intro section
Break the Intro section to 4 parts:
1. the kicker: starts the headline. ie: "how to", "the best"...
2. The Headline: the main title 
3. by-line: the smallest in the page
4. intro: the hoocker. same one shown on google results
    - should be distinct from body text


## Body Text
> Most people don't read. i they did. they only what they want
- First letter could be Big
- first sentence should be a shocker to entice the reader

- break text to multiple Headings
- each headline has 1-3 paragraph max. 
- each small paragraph could have sub-heading but it should be subtle but recognizable
- easier to scan and find exact info: use sub-headings
- Headings and sub-headings could be inside the paragraph like 1st line but with different style

- 45 to 80 character per line, spaces included

1. paragraph layout 
    - first line summurizes the pargraph
    - paragraph explains the idea
2. break it to multiple columns fo easier readability
    - lines in different columns should be aligned to each other

- Use Pull-Quote:
    - highlight Quote, interisting fact or a section summary 
    - could be in-between pargraphs. or in the side "margins"
    - should be eye catching
    - Could be on top of an image of who said it

## Images
- Captions 
    - should be on top of the image
    - consistent placement

- Person Image: put one of his Quotes on is if it makes sence with the article

- Don't mix styles in one aticle: 
    - eather illustrations, photography, info-graphics
    - be Consistent

## Ads
- Should be isolated 
- Distinct from content




## Resources
- [Anatomy Of Magazine Layout](https://youtu.be/7sSJtScnsjE)
