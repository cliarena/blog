{ inputs, ... }:
let
  inherit (inputs) nixpkgs devenv;
  systems = [
    "x86_64-linux"
    "i686-linux"
    "x86_64-darwin"
    "aarch64-linux"
    "aarch64-darwin"
  ];
  forAllSystems = f:
    builtins.listToAttrs (map (name: {
      inherit name;
      value = f name;
    }) systems);
in forAllSystems (system:
  let pkgs = import nixpkgs { inherit system; };
  in {
    default = devenv.lib.mkShell {
      inherit inputs pkgs;
      modules = [{
        env = { };
        processes = { };
        packages = with pkgs; [
          zola
          rustywind # sorts tailwindcss classes
          # html-tidy
          nodePackages_latest.vscode-html-languageserver-bin
          nodePackages_latest.tailwindcss
          nodePackages_latest.npm

          # deno # format js/ts files
        ];
        enterShell = ''
          npm i @egoist/tailwindcss-icons
          npm i -D @catppuccin/tailwindcss @iconify-json/material-symbols tailwindcss-opentype
        '';
      }];
    };
  })
