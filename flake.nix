{
  description = "Nixos configuration";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";

    devenv.url = "github:cachix/devenv";
  };

  outputs = inputs@{ ... }:
    let
      systems = [
        "x86_64-linux"
        "i686-linux"
        "x86_64-darwin"
        "aarch64-linux"
        "aarch64-darwin"
      ];
      forAllSystems = f:
        builtins.listToAttrs (map (name: {
          inherit name;
          value = f name;
        }) systems);

    in {

      devShells = import ./devenv.nix { inherit inputs; };
      apps = forAllSystems (system:

        let
          pkgs = import inputs.nixpkgs { inherit system; };
          concurrently = "${pkgs.concurrently}/bin/concurrently";
          tailwind = "${pkgs.nodePackages.tailwindcss}/bin/tailwindcss";
          zola = "${pkgs.zola}/bin/zola";
        in {
          dev = {
            type = "app";
            program = toString (pkgs.writers.writeBash "dev" ''
              ${concurrently} \
              "${tailwind} -i ./src/styles/tailwind.css -o ./src/static/styles.css -w" \
              "cd ./src && ${zola} serve --drafts" 
            '');
          };
        });
    };

}
