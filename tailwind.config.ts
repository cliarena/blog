import { getIconCollections, iconsPlugin } from "@egoist/tailwindcss-icons";
import catppuccin from "@catppuccin/tailwindcss";
import defaultTheme from "tailwindcss/defaultTheme";
import openType from "tailwindcss-opentype";



import type { Config } from "tailwindcss";

export default {
  content: ["./src/templates/**/*.html"],
  theme: {
    extend: {
      fontFamily: {
        'mono': ['JetBrains Mono', ...defaultTheme.fontFamily.mono],
        'sans': ['Space Grotesk', 'Inter','Barlow', 'Open Sans', 'El Messiri', ...defaultTheme.fontFamily.sans],
        'serif': ['Playfair', ...defaultTheme.fontFamily.serif],
      },
    },
  },
  plugins: [
    openType(),
    catppuccin({
      // prefix to use, e.g. `text-pink` becomes `text-ctp-pink`.
      // default is `false`, which means no prefix
      prefix: "ctp",
      // which flavour of colours to use by default, in the `:root`
      defaultFlavour: "macchiato",
    }),
    iconsPlugin({
      // Select the icon collections you want to use
      collections: getIconCollections(["material-symbols"]),
    }),
  ],
} satisfies Config;
